#-------------------------------------------------
#
# Project created by QtCreator 2014-04-27T11:51:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CustomTabView
TEMPLATE = app


SOURCES += main.cpp\
    TabBar.cpp \
    TabWidget.cpp \
    TestWidget.cpp \
    MainWindow.cpp

HEADERS  += \
    TabBar.h \
    TabWidget.h \
    TestWidget.h \
    MainWindow.h

FORMS    += \
    MainWindow.ui \
    TestWidget.ui
