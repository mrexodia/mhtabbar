#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mTab1 = new TestWidget(this);
    mTab1->setLabelText("Tab 1");
    mTab2 = new TestWidget(this);
    mTab2->setLabelText("Tab 2");
    mTab3 = new TestWidget(this);
    mTab3->setLabelText("Tab 3");

    mTabWidget = new MHTabWidget(NULL);
    mTabWidget->addTab(mTab1, "Tab 1");
    mTabWidget->addTab(mTab2, "Tab 2");
    mTabWidget->addTab(mTab3, "Tab 3");

    setCentralWidget(mTabWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionE_xit_triggered()
{
    this->close();
}
