#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "TabWidget.h"
#include "testwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionE_xit_triggered();

private:
    Ui::MainWindow *ui;
    MHTabWidget* mTabWidget;
    TestWidget* mTab1;
    TestWidget* mTab2;
    TestWidget* mTab3;
};

#endif // MAINWINDOW_H
