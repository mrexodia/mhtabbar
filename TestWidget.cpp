#include "testwidget.h"
#include "ui_testwidget.h"

TestWidget::TestWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TestWidget)
{
    ui->setupUi(this);
}

void TestWidget::setLabelText(QString text)
{
    ui->label->setText(text);
}

TestWidget::~TestWidget()
{
    delete ui;
}
